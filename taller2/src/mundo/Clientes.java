package mundo;

import java.util.Date;

public class Clientes extends Trabajo
{
	public enum NegocioCliente
	{

		AUTOPAN,
		OMA,
		ROCKSTAR,
		KRAFT,
		BIOWARE
		
	}
	
	protected NegocioCliente negocioCliente;

	public Clientes(Date pFecha, String pLugar, boolean pObligatorio,
			boolean pFormal, TrabajoEventos pTrabajo, NegocioCliente pNegocio) {
		super(pFecha, pLugar, pObligatorio, pFormal, pTrabajo);
		// TODO Auto-generated constructor stub
		negocioCliente = pNegocio;
	}
	
	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}
	
	public String toString()
	{
	  return "Evento con Clientes: \n"
	  		+"FECHA: " + this.fecha
	  		+"\nLUGAR: " + this.lugar
	  		+"\nTIPO EVENTO: " + this.trabajoEventos
	  		+"\nOBLIGATORIO: " + conv(this.obligatorio)
	  		+"\nFORMAL: "+ conv(this.formal);
	}

}
