package mundo;

import java.util.Date;

import mundo.Trabajo.TrabajoEventos;

public class Socios extends Trabajo 
{
	public enum EmpresasSocio
	{

		ECOPETROL,
		NINTENDO,
		PORNHUB,
		FOX,
		SAMSUMG
		
		
	}
	
	protected EmpresasSocio empresasSocio;

	public Socios(Date pFecha, String pLugar, boolean pObligatorio,
			boolean pFormal, TrabajoEventos pTrabajo, EmpresasSocio pEmpresa) {
		super(pFecha, pLugar, pObligatorio, pFormal, pTrabajo);
		empresasSocio = pEmpresa;
		// TODO Auto-generated constructor stub
	}
	
	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}
	
	public String toString()
	{
	  return "Evento con Socios: \n"
	  		+"FECHA: " + this.fecha
	  		+"\nLUGAR: " + this.lugar
	  		+"\nTIPO EVENTO: " + this.trabajoEventos
	  		+"\nOBLIGATORIO: " + conv(this.obligatorio)
	  		+"\nFORMAL: "+ conv(this.formal);
	}

}
