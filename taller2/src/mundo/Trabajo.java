package mundo;

import java.util.Date;

public class Trabajo extends Evento
{
	public enum TrabajoEventos
	{
		JUNTAS,
		ALMUERZOS,
		COCTELES,
		CENAS,
		PRESENTACIONES,
		TALLERES,
		REPORTES,
		CAPACITACION
		
	}
    
	protected TrabajoEventos trabajoEventos;
	

	public Trabajo(Date pFecha, String pLugar, boolean pObligatorio,
			boolean pFormal, TrabajoEventos pTrabajo) {
		super(pFecha, pLugar, pObligatorio, pFormal);
		trabajoEventos = pTrabajo;
		// TODO Auto-generated constructor stub
	}

}
